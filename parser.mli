type token =
  | INT of (int)
  | SYMBOL of (string)
  | STRING of (string)
  | LPAREN
  | RPAREN
  | LBRACKET
  | RBRACKET
  | DOT
  | EOF

val main :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> Types.scheme_object
