#!/bin/bash
ast=`cat /dev/stdin`

ocaml -noprompt <<EOF | tee back.out | sed -n '/- : Types.scheme_object =/,$ p' | sed -e 's/.*Types.scheme_object.*= //'
open Types;;
open Evaluator;;
open Environment;;
#load "types.cmo";;
#load "environment.cmo";;
#load "evaluator.cmo";;

let eval1 obj =
  Evaluator.eval Environment.initial_env obj (fun o -> o);;

eval1 ($ast);;
exit 0;;
EOF

