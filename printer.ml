open Types;;
open Format;;

module Printer = struct
  let rec display ff obj =
    let print_int = fprintf ff "%d" in
    let print_string = fprintf ff "%s" in
    let rec print_separated = function
        [] -> print_string ""
      | [x] -> display ff x
      | x::y -> (display ff x); print_string " "; (print_separated y)
    in
    match obj with
        Int (i) -> print_int i
      | String (s) -> print_string ("\""^s^"\"")
      | Symbol (s) -> print_string s
      | Null -> print_string "()"
      | True -> print_string "#t"
      | False -> print_string "#f"
      | Closure (_, _, _) -> print_string "#<closure>"
      | Continuation (_) -> print_string "#<continuation>"
      | Quotation x -> display ff x
      | ProperList (l) ->
        open_hovbox 1;
        print_string "("; print_separated l; print_string ")"; close_box()
      | _ -> print_string "#<unknown>"
        
  let rec write ff obj =
    let print_int = fprintf ff "%d" in
    let print_string = fprintf ff "%s" in
    let rec print_separated = function
        [] -> print_string ""
      | [x] -> write ff x
      | x::y -> (display ff x); print_string " "; (print_separated y)
    in
    match obj with
        Int (i) -> print_int i
      | String (s) 
      | Symbol (s) -> print_string s
      | Null -> print_string "()"
      | True -> print_string "#t"
      | False -> print_string "#f"
      | Closure (_, _, _) -> print_string "#<closure>"
      | Continuation (_) -> print_string "#<continuation>"
      | Quotation x -> display ff x
      | ProperList (l) ->
        open_hovbox 1;
        print_string "("; print_separated l; print_string ")"; close_box()
      | _ -> print_string "#<unknown>"

        
end
