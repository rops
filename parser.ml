type token =
  | INT of (int)
  | SYMBOL of (string)
  | STRING of (string)
  | LPAREN
  | RPAREN
  | LBRACKET
  | RBRACKET
  | DOT
  | EOF

open Parsing;;
# 1 "parser.mly"

  open Types;;
# 17 "parser.ml"
let yytransl_const = [|
  260 (* LPAREN *);
  261 (* RPAREN *);
  262 (* LBRACKET *);
  263 (* RBRACKET *);
  264 (* DOT *);
    0 (* EOF *);
    0|]

let yytransl_block = [|
  257 (* INT *);
  258 (* SYMBOL *);
  259 (* STRING *);
    0|]

let yylhs = "\255\255\
\001\000\003\000\003\000\003\000\003\000\004\000\004\000\002\000\
\002\000\002\000\002\000\000\000"

let yylen = "\002\000\
\001\000\003\000\003\000\006\000\006\000\000\000\002\000\001\000\
\001\000\001\000\001\000\002\000"

let yydefred = "\000\000\
\000\000\000\000\011\000\009\000\010\000\006\000\006\000\012\000\
\001\000\008\000\000\000\000\000\002\000\000\000\003\000\000\000\
\000\000\000\000\000\000\000\000\004\000\005\000"

let yydgoto = "\002\000\
\008\000\009\000\010\000\011\000"

let yysindex = "\001\000\
\033\255\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\021\255\007\255\000\000\251\254\000\000\252\254\
\033\255\033\255\000\255\005\255\000\000\000\000"

let yyrindex = "\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\027\255\000\000\014\255\
\000\000\000\000\000\000\000\000\000\000\000\000"

let yygindex = "\000\000\
\000\000\245\255\000\000\012\000"

let yytablesize = 39
let yytable = "\014\000\
\016\000\001\000\017\000\018\000\021\000\019\000\020\000\003\000\
\004\000\005\000\006\000\022\000\007\000\015\000\007\000\007\000\
\007\000\007\000\012\000\007\000\007\000\003\000\004\000\005\000\
\006\000\013\000\007\000\007\000\007\000\007\000\007\000\007\000\
\007\000\003\000\004\000\005\000\006\000\000\000\007\000"

let yycheck = "\011\000\
\012\000\001\000\008\001\008\001\005\001\017\000\018\000\001\001\
\002\001\003\001\004\001\007\001\006\001\007\001\001\001\002\001\
\003\001\004\001\007\000\006\001\007\001\001\001\002\001\003\001\
\004\001\005\001\006\001\001\001\002\001\003\001\004\001\005\001\
\006\001\001\001\002\001\003\001\004\001\255\255\006\001"

let yynames_const = "\
  LPAREN\000\
  RPAREN\000\
  LBRACKET\000\
  RBRACKET\000\
  DOT\000\
  EOF\000\
  "

let yynames_block = "\
  INT\000\
  SYMBOL\000\
  STRING\000\
  "

let yyact = [|
  (fun _ -> failwith "parser")
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 13 "parser.mly"
             ( _1 )
# 99 "parser.ml"
               : Types.scheme_object))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'exprlist) in
    Obj.repr(
# 17 "parser.mly"
                                                  (  ProperList (List.rev _2) )
# 106 "parser.ml"
               : 'list))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'exprlist) in
    Obj.repr(
# 18 "parser.mly"
                                                  (  ProperList (List.rev _2) )
# 113 "parser.ml"
               : 'list))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 4 : 'exprlist) in
    let _3 = (Parsing.peek_val __caml_parser_env 3 : 'expr) in
    let _5 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 19 "parser.mly"
                                                  (  ImproperList ((List.rev (_3::_2)), _5) )
# 122 "parser.ml"
               : 'list))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 4 : 'exprlist) in
    let _3 = (Parsing.peek_val __caml_parser_env 3 : 'expr) in
    let _5 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 20 "parser.mly"
                                                  (  ImproperList ((List.rev (_3::_2)), _5) )
# 131 "parser.ml"
               : 'list))
; (fun __caml_parser_env ->
    Obj.repr(
# 24 "parser.mly"
                                                  (  []; )
# 137 "parser.ml"
               : 'exprlist))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'exprlist) in
    let _2 = (Parsing.peek_val __caml_parser_env 0 : 'expr) in
    Obj.repr(
# 25 "parser.mly"
                                                  (  _2::_1 )
# 145 "parser.ml"
               : 'exprlist))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'list) in
    Obj.repr(
# 29 "parser.mly"
                                                  (  _1; )
# 152 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 30 "parser.mly"
                                                  (  Symbol _1 )
# 159 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : string) in
    Obj.repr(
# 31 "parser.mly"
                                                  (  String _1 )
# 166 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : int) in
    Obj.repr(
# 32 "parser.mly"
                                                  (  Int _1; )
# 173 "parser.ml"
               : 'expr))
(* Entry main *)
; (fun __caml_parser_env -> raise (Parsing.YYexit (Parsing.peek_val __caml_parser_env 0)))
|]
let yytables =
  { Parsing.actions=yyact;
    Parsing.transl_const=yytransl_const;
    Parsing.transl_block=yytransl_block;
    Parsing.lhs=yylhs;
    Parsing.len=yylen;
    Parsing.defred=yydefred;
    Parsing.dgoto=yydgoto;
    Parsing.sindex=yysindex;
    Parsing.rindex=yyrindex;
    Parsing.gindex=yygindex;
    Parsing.tablesize=yytablesize;
    Parsing.table=yytable;
    Parsing.check=yycheck;
    Parsing.error_function=parse_error;
    Parsing.names_const=yynames_const;
    Parsing.names_block=yynames_block }
let main (lexfun : Lexing.lexbuf -> token) (lexbuf : Lexing.lexbuf) =
   (Parsing.yyparse yytables 1 lexfun lexbuf : Types.scheme_object)
;;
# 34 "parser.mly"

# 200 "parser.ml"
