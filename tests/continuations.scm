(define assert-equal
  (lambda (x y)
    (if (= x y)
        x
        (error "ASSERTION FAILED. Expected " x " got " y))))

;;----------------------------------------------------------------------
;; one-shot, outward
(define res (+ 1
               (call/cc (lambda (k)
                          (k 2)))
               3)) 

res
(assert-equal 6 res)

;;----------------------------------------------------------------------
;; re-capture
(define retry 1)
(define res2 (call/cc (lambda (k)
                        (set! retry k)
                        (k (quote first-time)))))

(if (= res2 (quote first-time))
    (retry (quote second-time))
    (assert-equal res2 (quote second-time)))

