(define assert-equal
  (lambda (x y)
    (if (= x y)
        (quote ok)
        (error "ASSERTION FAILED. Expected " x " got " y))))

(define x 1)

;;; Closes over x
(define add-x (lambda (y) (+ x y)))

(set! x 3)
(assert-equal (add-x 2) 5) 

(define set-free-x
  (lambda ()
     (set! x 5)))

(set-free-x)
(assert-equal (add-x 3) 8)

;;; X is bounded: should not modify outer environment
(define set-bounded-x
  (lambda (x) (set! x 2)))

(set-bounded-x "dummy")
(assert-equal (add-x 3) 8) 

(set! x 0)
(define call-set-free-x
  (lambda ()
    (set-free-x)))
(call-set-free-x)
(assert-equal x 5)

