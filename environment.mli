open Types;;
module Environment :
sig
  val push_env_frame : scheme_environment -> symbol list -> scheme_object list -> scheme_environment
  val lookup : scheme_environment -> string -> scheme_object
  val update : scheme_environment -> string -> scheme_object -> unit
  val current_env_frame : scheme_environment -> environment_frame
  val initial_env : scheme_environment
end
