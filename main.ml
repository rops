open Types;;
open Environment;;
open Evaluator;;
open Printer;;
open Format;;

let print_prompt () =
  print_newline ();
  print_string "~> "; 
  print_flush ();;

let print_obj obj =
  Printer.display Format.std_formatter obj; print_flush ();;

let rec print_obj_list ol =
  match ol with
      [] -> ()
    | [one] -> print_obj one
    | more::than::one -> print_obj more; print_string ", "; print_obj_list (than::one);;

let rec repl lexbuf state =
  let ast = Parser.main Lexer.token lexbuf in
  let next_state = Evaluator.eval ({ state with Evaluator.cont=[ast]; Evaluator.pending_results=[[]] }) in
  (match next_state.Evaluator.pending_results with
      [[res]] -> print_obj res
    | [[]] -> ()
    | [] -> print_string "empty"
    | [lres] ->
      print_string "Multiple: ";
      print_obj_list lres
    | lol ->
      failwith "Unknown result";
      List.iter print_obj_list lol;
      print_flush());
  print_prompt ();
  repl lexbuf next_state;;
    
let _ =
  try
    let lexbuf = Lexing.from_channel stdin in
    print_prompt ();
    repl lexbuf { Evaluator.env = Environment.initial_env; 
                  Evaluator.pending_results = [[]]; 
                  Evaluator.cont = [] }
  with
      Lexer.Eof ->
        flush stdout;
        exit 0
    | Scheme_user_error (l) ->
      List.iter (Printer.write Format.std_formatter) l;
      exit 1 ;;
