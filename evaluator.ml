(*
  
  This attempt will not work as it is now. The evaluator should be
  CPS, what we can try is to add some "instruction" objects to the
  cont list (not scheme objects, but control stuff) that can be used
  to ask the evaluator to perform pending instructions.

  I think this would be enough:

  Rewind (application and call/cc) re-evaluates the last result, applying the last rib and popping a result frame.
  RestoreEnv (Env) Restore an environment (return from a closure)
  Cond of schemeobj * schemeobj If the last result is true, pile up the true case...

  Bind(var)  per  a define
  Set(var) per a set!

  This is a bit ugly... looks like mixing a compiler and an interpreter...
*)

open Environment;;
open Printer ;;                         (* Just for debugging *)
open Types;;

module Evaluator = struct
(*********************************************************************
                                 AUX
**********************************************************************)

let (|>) (p : 'a) (f : 'a -> 'b) = f p;;

let rec prefix0 n l acum = 
  match n with
      0 -> List.rev acum
    | i -> prefix0 (n - 1) (List.tl l) ((List.hd l)::acum);;
let prefix n l = prefix0 n l [];;

let rec split_at0 n (seen, unseen) =
  match n with
      0 -> ((List.rev seen), unseen)
    | i -> split_at0 (n - 1) (((List.hd unseen)::seen), (List.tl unseen));;
let split_at n l = split_at0 n ([], l);;

let rec loop_acum n f initial =
  match n with
      0 -> initial
    | i -> loop_acum (i - 1) f (f initial);;

let is_some = function
    Some x -> true
  | None -> false

let is_none x = not (is_some x);;

let get_some = function
    Some x -> x
  | None -> raise Not_found

let eval_error s = raise (Scheme_eval_error s);;

let get_int = function
    Int i -> i
  | _ -> raise Scheme_cast_error;;

let get_bool = function
    True -> true
  | False -> false
  | _ -> raise Scheme_cast_error;;

let scheme_obj_of_bool b =
  match b with
      true -> True
    | false -> False;;

(**********************************************************************
			       BUILTINS
 **********************************************************************)
let make_arith_op_builtin_func (base:int) (f:int -> int -> int) params =
        Some (List.fold_left (fun x -> fun y -> Int (f (get_int x) (get_int y))) (Int base) params) ;;

let builtin_begin params =
  match (List.rev params) with
      hd::tl -> Some hd
    | [] -> None;; 

let builtin_equal (params:scheme_object list) : (scheme_object option) =
  Some (scheme_obj_of_bool (List.for_all (( = ) (List.hd params)) (List.tl params)));;

let builtin_option (o:string) : ((scheme_object list) -> (scheme_object option) ) option =
  match o with
    "+" -> Some (make_arith_op_builtin_func 0 ( + ))
  | "-" -> Some (make_arith_op_builtin_func 0 ( - ))
  | "*" -> Some (make_arith_op_builtin_func 1 ( * ))
  | "/" -> Some (make_arith_op_builtin_func 1 ( / ))
  | "=" -> Some (builtin_equal)
  | "begin" -> Some (builtin_begin)
  | "list" -> Some (fun params -> (Some (ProperList params)))
  | _ -> None
;;

(**********************************************************************
				 CPS
 **********************************************************************)

(*

In a continuation, maybe we also need to save the rib...
if so, The Continuation object might need to save almost a whole state...

Then the state could be a stack of frames, instead of a record of stacks...

*)

type eval_state = { cont : scheme_object list;
                    pending_results : scheme_object list list;
                    env : scheme_environment };;

let rec eval (state:eval_state) : eval_state =
  let (current_res_frame, older_res_frames) = (List.hd state.pending_results, List.tl state.pending_results) in
  let push_result results res = (res::(List.hd results))::(List.tl results) in
  let rewind s =
    { s with pending_results = ((s.pending_results |> List.hd |> List.tl)::(List.tl s.pending_results));
      cont = (s.pending_results |> List.hd |> List.hd)::s.cont }
  in
  match state.cont with
      exp::rest ->
        let return obj =
          eval { state with pending_results = (push_result state.pending_results obj); cont=rest }
        in
        (match exp with
          | ProperList (Symbol "quote"::[obj]) ->
            return obj
          | ProperList (Symbol "error"::whatever) ->
            raise (Scheme_user_error whatever)
          | ProperList (Symbol "define"::Symbol var::[expression])  ->
            eval { state with cont = expression::Bind(var)::rest;
              pending_results = []::state.pending_results }
          | Bind (var) ->             
            let f = Environment.current_env_frame state.env in
            Hashtbl.add f var (List.hd current_res_frame);
            eval { state with cont=rest;
              pending_results= older_res_frames }
          | ProperList (Symbol "set!"::Symbol var::[expression])  ->
            eval { state with cont = expression::Set(var)::rest;
              pending_results = []::state.pending_results }
          | Set (var) ->
            Environment.update state.env var (List.hd current_res_frame);
            eval { state with cont=rest;
              pending_results= older_res_frames }
          | ProperList (Symbol "lambda"::formals::body) ->
            let proper_body = ProperList (Symbol "begin"::body) in
            let syms = match formals with
                ProperList l -> (List.map (function 
	        (Symbol s) -> s
	          | _ -> eval_error "Invalid formal parameter list") l)
              | _ -> eval_error "Bad lambda syntax"
            in
            return (Closure (proper_body, state.env, syms))
          | ProperList (Symbol "call/cc"::[callee]) ->
            let continuation = Continuation (state.env, rest, state.pending_results) in
            eval { state with cont = callee::Rewind::rest; (* (EvalSeq 1):: *)
              pending_results = [continuation]::state.pending_results }
          | ProperList (Symbol "if"::test::true_case::[false_case]) ->
            eval { state with cont = test::Cond(true_case, false_case)::rest;
              pending_results = []::state.pending_results }
          | Cond (true_case, false_case) ->
            (match (List.hd current_res_frame) with
                False ->
                  eval { state with cont = false_case::rest;
                    pending_results = older_res_frames }
              | _ -> eval { state with cont = true_case::rest;
                          pending_results = older_res_frames })
          | ProperList (f::params) ->
            eval { state with cont= params @ (f::Rewind::rest); (* (EvalSeq len):: *)
              pending_results = []::state.pending_results }
          | Rewind ->
            eval (rewind { state with cont = rest })
          | Closure (body, closure_env, formals) ->
            let env = (Environment.push_env_frame closure_env formals current_res_frame) in
            eval { env; cont=body::(RestoreEnv state.env)::rest; pending_results = older_res_frames }
          | RestoreEnv (env) ->
            eval { state with env; cont = rest }
          | Continuation (saved_env, saved_cont, saved_res) ->
            eval { env=saved_env;
                   cont= saved_cont; 
                   pending_results = push_result saved_res (List.hd current_res_frame) }
          | Symbol op when is_some (builtin_option op) ->
	    let fbuiltin = op |> builtin_option |> get_some in
            return (Builtin fbuiltin)
          | Builtin (fbuiltin) ->
            (match fbuiltin current_res_frame with
                Some (result) -> 
                  eval { state with pending_results = push_result older_res_frames result;
                    cont = rest }
              | None -> 
                eval { state with cont = rest;
                  pending_results = older_res_frames })
          | Symbol s ->
            return (Environment.lookup state.env s)
          | whatever ->
            return whatever) (* Int, String, Quotation, Null *)
    | [] -> state
;;



end


  
