module Printer :
sig
  
  val display : Format.formatter -> Types.scheme_object -> unit
  val write : Format.formatter -> Types.scheme_object -> unit    
  
end
