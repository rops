%{
  open Types;;
%}

%token <int> INT
%token <string> SYMBOL STRING 
%token LPAREN RPAREN LBRACKET RBRACKET DOT EOF

%start main
%type <Types.scheme_object> main

%%
main :  expr { $1 } /* exprlist EOF { List.rev $1 } */
  ;

list
    : LPAREN exprlist RPAREN                      {  ProperList (List.rev $2) }
    | LBRACKET exprlist RBRACKET                  {  ProperList (List.rev $2) }
    | LPAREN exprlist expr DOT expr RPAREN        {  ImproperList ((List.rev ($3::$2)), $5) }
    | LBRACKET exprlist expr DOT expr RBRACKET    {  ImproperList ((List.rev ($3::$2)), $5) }
    ;

exprlist
    :                                             {  []; }
    |  exprlist expr                              {  $2::$1 }
    ;   

expr
    : list                                        {  $1; }
    | SYMBOL                                      {  Symbol $1 }
    | STRING                                      {  String $1 }
    | INT                                         {  Int $1; }
    ;
%%
