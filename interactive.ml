open Types;;
open Evaluator;;
open Environment;;
open Lexer;;
open Parser;;
#load "types.cmo";;
#load "environment.cmo";;
#load "evaluator.cmo";;
#load "lexer.cmo";;
#load "parser.cmo";;

let (|>) (p : 'a) (f : 'a -> 'b) = f p;;

#untrace Evaluator.eval;;
#load "evaluator.cmo";;

let state = ref { Evaluator.env = Environment.initial_env;
                  Evaluator.cont=[];
                  Evaluator.pending_results=[[]] };;
let go exp =
  state := Evaluator.eval {
    !state with Evaluator.cont = [exp];
    Evaluator.pending_results = [[]] };
  state;;
#trace Evaluator.eval;;

let read s = Parser.main Lexer.token (Lexing.from_string s);;
let rev x = go (read x);;

(* "(+ 1 (+ 1 (call/cc (lambda (k) (k 3))) 5) 3)" |> rev;;  *)

(* "(+ 1 1)" |> rev;;  *)
(* "(define x 1)" |> rev;; *)
(* "(define add-x (lambda (y) (+ x y)))" |> rev;;  *)
(* "(add-x 1)" |> rev;;  *)
