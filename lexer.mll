(*
  Scanner For A Scheme
  
  Known problems: 1s gets parsed as (Int 1) + (Symbol s). We should probably introduce a rule for invalid numbers that throws an error.

 *)
{
open Parser;;

exception Eof;;

let incr_linenum lexbuf =
  let pos = lexbuf.Lexing.lex_curr_p in
  lexbuf.Lexing.lex_curr_p <- { pos with
				Lexing.pos_lnum = pos.Lexing.pos_lnum + 1;
				Lexing.pos_bol = pos.Lexing.pos_cnum;
			      };;

let make_string x = STRING (String.sub x 1 ((String.length x) - 2));;

}

let newline = ['\n']
let spaces = ['\r' '\t' ' ']
let letter = ['a'-'z' 'A'-'Z']
let digit = ['0'-'9']
let arith_ops = ['*' '/' '+' '-' '=']
let punctuation = ['!' '#'  '$'  '%'  '&'  '|'  '*'  
	           '+' '-'  '/'  ':'  '<'  '='  '>'  '?'  
	           '@'  '^'  '_'  '~'  '\"']

let delimiter = "\n\r"|['[' ']' '(' ')' '"' ';' '#' '\r' '\n' '\t' ' ']
  
let symbol = letter+ (digit|punctuation|letter)*
let scheme_string = '"' (('\\' _ )|[^ '"'])* '"'

rule token = parse
| '\n' 
    {
      incr_linenum lexbuf; 
      token lexbuf
    }
| scheme_string as s  { make_string s }
| spaces+ { token lexbuf }
| digit+ as inum 
    {
      INT (int_of_string inum)
    }
| arith_ops as op { SYMBOL (Char.escaped op) }
| symbol as s { SYMBOL s }
| ';' [^ '\n']* { token lexbuf }	(* eat up one-line comments *)
| '[' { LBRACKET  }
| ']' { RBRACKET }
| '(' { LPAREN }
| ')' { RPAREN }
| eof { raise Eof }
