open Types;;
module Environment = struct
(**********************************************************************
                             ENVIRONMENT
**********************************************************************)
let push_env_frame env formals actuals =
  
  let h = Hashtbl.create (List.length formals) in
    List.iter
      (function (name, obj) -> Hashtbl.add h name obj )
      (List.combine formals actuals);
    h::env
;;

let find_frame env k =
  try 
    List.find (fun x -> Hashtbl.mem x k) env
  with
      Not_found -> raise (Scheme_eval_error ("undefined: "^k));;

let lookup env k =
  try 
    Hashtbl.find  (find_frame env k) k
  with
      Not_found -> raise (Scheme_eval_error ("undefined: "^k));;      

let update env k v =
  try 
    Hashtbl.replace (find_frame env k) k v
  with
      Not_found -> raise (Scheme_eval_error ("undefined: "^k));;            

let current_env_frame  = function
    (e::rest) -> e
  | [] -> assert false
;;

let initial_env = [Hashtbl.create 1]
  
end
