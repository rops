(*********************************************************************
                                 AUX
**********************************************************************)

let (|>) (p : 'a) (f : 'a -> 'b) = f p;;

let is_some = function
    Some x -> true
  | None -> false

let is_none x = not (is_some x);;

let get_some = function
    Some x -> x
  | None -> raise Not_found

exception Scheme_cast_error;;

exception Scheme_eval_error of string;;
let eval_error s = raise (Scheme_eval_error s);;

type symbol = string ;;

(* 
   A scheme object is anything that can be considered a value in Scheme. Notice that: 
   
   - Any symbolic expression can be an object (through the use of quote).
   - Some objects have a literal representation in the program (can be read), but others don't: Closures and Continuations are created through evaluation of some other expressions.

 *)
type scheme_object =
    Int of int
  | String of string
  | Symbol of symbol
  | Null
  | True
  | False
  | Quotation of scheme_object
  | ProperList of scheme_object list
  | ImproperList of (scheme_object list) * scheme_object 
  | Closure of scheme_object * scheme_environment * (symbol list)
  | Continuation of scheme_environment * (scheme_object -> scheme_object)
and 
  environment_frame = (string, scheme_object) Hashtbl.t 
and 
  scheme_environment = environment_frame list
;;

let get_int = function
    Int i -> i
  | _ -> raise Scheme_cast_error;;

(**********************************************************************
                             ENVIRONMENT
**********************************************************************)
let env_from_alist alist =
  let h = Hashtbl.create 1 in
  List.iter (fun (k, v) -> Hashtbl.add h k v) alist;
  h
;;

let push_env_frame env formals actuals =
  let h = Hashtbl.create (List.length formals) in
    List.iter
      (function (name, obj) -> Hashtbl.add h name obj )
      (List.combine formals actuals);
    h::env
;;

let lookup env k =
    Hashtbl.find (List.find (fun x -> Hashtbl.mem x k) env) k ;; 

let current_env_frame  = function
    (e::rest) -> e
  | [] -> assert false
;;

(**********************************************************************
			       BUILTINS
 **********************************************************************)
let make_arith_op_builtin_func (base:int) (f:int -> int -> int) =
        List.fold_left (fun x -> fun y -> Int (f (get_int x) (get_int y))) (Int base) ;;

let builtin_begin params = List.hd (List.rev params)

let builtin_option = function
    "+" -> Some (make_arith_op_builtin_func 0 ( + ))
  | "-" -> Some (make_arith_op_builtin_func 0 ( - ))
  | "*" -> Some (make_arith_op_builtin_func 1 ( * ))
  | "/" -> Some (make_arith_op_builtin_func 1 ( / ))
  | "begin" -> Some builtin_begin
  | _ -> None
;;

(**********************************************************************
                           EVALUATION 
 **********************************************************************)
let rec eval (e:scheme_environment) (exp:scheme_object) =
  let exp_eval = (eval e) in
    match exp with
      | ProperList (Symbol "define"::Symbol var::cdr)  ->
          let f = current_env_frame e in
            Hashtbl.add f var (exp_eval (ProperList ((Symbol "begin")::cdr)));
            Null
      | ProperList (Symbol "lambda"::formals::body) ->
          let proper_body = ProperList (Symbol "begin"::body) in
          let syms = match formals with
              ProperList l -> (List.map (function 
	                                     (Symbol s) -> s
	                                 | _ -> eval_error "Invalid formal parameter list") l)
          | _ -> eval_error "Bad lambda syntax"
        in
          Closure (proper_body, e, syms)
    | ProperList (Symbol "if"::test::true_case::[false_case]) ->
        (match (exp_eval test) with
	     False -> exp_eval false_case
           | _ -> exp_eval true_case)
    | ProperList (f::params) ->
        let evaluated_params = (List.map exp_eval params) in
          (match f with
	       Symbol op when is_some (builtin_option op) ->
	         let fbuiltin = op |> builtin_option |> get_some in
	           (fbuiltin evaluated_params)
             | Closure (body, closure_env, formals) ->
                 eval (push_env_frame closure_env formals evaluated_params) body
             | _ -> eval_error "Cannot apply non-object" )
    | Symbol s ->
        lookup e s
    | whatever -> whatever (* Int, String, Quotation, Null *)
;;

(**********************************************************************
				 CPS
 **********************************************************************)
let rec eval_cps (e:scheme_environment) (exp:scheme_object) (return:scheme_object -> scheme_object) =
  let exp_eval = eval_cps e in
    match exp with
      | ProperList (Symbol "define"::Symbol var::[expression])  ->
          exp_eval expression (fun expression_value ->
                                 let f = current_env_frame e in
                                   Hashtbl.add f var expression_value;
                                   return Null)
      | ProperList (Symbol "lambda"::formals::body) ->
          let proper_body = ProperList (Symbol "begin"::body) in
          let syms = match formals with
              ProperList l -> (List.map (function 
	                                     (Symbol s) -> s
	                                   | _ -> eval_error "Invalid formal parameter list") l)
            | _ -> eval_error "Bad lambda syntax"
          in
            return (Closure (proper_body, e, syms))
      | ProperList (Symbol "call/cc"::[callee]) ->
          let continuation = Continuation (e, return) in
            eval_application e callee [continuation] return
      | ProperList (Symbol "if"::test::true_case::[false_case]) ->
          exp_eval test (function
	                     False -> exp_eval false_case return
                           | _ -> exp_eval true_case return)
      | ProperList (f::params) ->
          eval_list_cps e (ProperList params) 
            (function
                 ProperList evaluated_params -> 
                   (eval_application e f evaluated_params return)
               | _ -> invalid_arg "eval_list_cps did not return a proper list object")
    | Symbol s ->
        return (lookup e s)
    | whatever ->
        return whatever (* Int, String, Quotation, Null *)
and eval_list_cps (env:scheme_environment) (exp:scheme_object) (return: scheme_object -> scheme_object) =
  match exp with
      ProperList [] as base -> return base
    | ProperList (car::cdr) ->
        eval_cps env car 
	  (fun (car_result:scheme_object) -> 
	     (eval_list_cps env (ProperList cdr)
	        (function 
		     ProperList (cdr_result) -> return (ProperList (car_result::cdr_result))
	           |_ -> assert false)))
    | _ -> invalid_arg "eval_list_cps called with a non-list scheme object"

and eval_application e f evaluated_params return =
  match f with
      Symbol op when is_some (builtin_option op) ->
	let fbuiltin = op |> builtin_option |> get_some in
	  return (fbuiltin evaluated_params)
    | _ -> eval_cps e f (function
                           | Closure (body, closure_env, formals) ->
                               eval_cps (push_env_frame closure_env formals evaluated_params) body return
                           | Continuation (e, escape_proc) ->
                               (escape_proc (List.hd evaluated_params))
                           | _ -> eval_error "Trying to apply something that is not a closure")
;;


(**********************************************************************)
let e = env_from_alist [("a", (Int 1));
                        ("b", (Int 2));
                        ("true-val", True);
                       ];;


let print_res = function
    Int i -> (Printf.printf "Res: %d\n" i); Int i
  | _ -> assert false ;;

let eval0 = eval [e];;
let eval1 obj = eval_cps [e] obj print_res;;

assert ((eval1 (ProperList [Symbol "+";Int 2; Symbol "a"])) = Int 3);;
assert ((eval1 (ProperList [Symbol "begin"; Int 1; Int 2])) = Int 2);;

(***********************************************************************)
eval1 (Int 1);;

(eval1 (ProperList [ Symbol "if";
                     Quotation (Symbol "true-val");
                     Int 1;
                     Int 0; ]));;

((eval1 (ProperList [Symbol "+";Int 2; Symbol "a"])) = Int 3);;
(eval1 (ProperList [ Symbol "begin";
		     ProperList [Symbol "define"; Symbol "x"; Int 3];
		     Symbol "x"]))
