open Types;;
module Evaluator :
sig

  type eval_state = { cont : scheme_object list;
                      pending_results : scheme_object list list;
                      env : scheme_environment };;

val eval : eval_state -> eval_state;;

end
