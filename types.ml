type symbol = string ;;

type scheme_object =
    Int of int
  | String of string
  | Symbol of symbol
  | Null
  | True
  | False
  | Quotation of scheme_object
  | ProperList of scheme_object list
  | ImproperList of (scheme_object list) * scheme_object 
  | Closure of scheme_object * scheme_environment * (symbol list)
  | Builtin of ((scheme_object list) -> (scheme_object option))
  | Continuation of scheme_environment * (scheme_object list) * (scheme_object list list)
  | RestoreEnv of scheme_environment
  | Rewind
  | Cond of (scheme_object * scheme_object)
  | Set of string
  | Bind of string
and 
  environment_frame = (string, scheme_object) Hashtbl.t 
and 
  scheme_environment = environment_frame list
;;

exception Scheme_cast_error;;
exception Scheme_eval_error of string;;
exception Scheme_user_error of scheme_object list;;
